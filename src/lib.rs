use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;

#[derive(Debug, Clone, Eq, PartialEq)]
enum Node {
    Leaf { value: char, priority: usize },
    Internal { children: Vec<Node>, priority: usize },
}

impl Ord for Node {
    fn cmp(&self, other: &Node) -> Ordering {
        other.priority().cmp(&self.priority())
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

trait Popper {
    fn pop_by(&mut self, count: usize) -> Vec<Node>;
}

impl Popper for BinaryHeap<Node> {
    fn pop_by(&mut self, arity: usize) -> Vec<Node> {
        let mut children = Vec::new();

        for _ in 0..arity {
           children.push(self.pop().unwrap());
        }

        children
    }
}

impl Node {
    fn priority(&self) -> usize {
        match *self {
            Node::Leaf { priority, .. } => priority,
            Node::Internal { priority, .. } => priority,
        }
    }

    fn value(&self) -> char {
        match *self {
            Node::Leaf { value, .. } => value,
            Node::Internal { .. } => '0',
        }
    }

    fn encode<'a>(&'a self, prefix: Option<&'a str>) {
        let prefix = prefix.unwrap_or("");

        match *self {
            Node::Leaf { .. } => println!("{}\t {}\t {}", &self.value(), &self.priority(), prefix),
            Node::Internal { ref children, .. } => {
                for (i, child) in children.iter().enumerate() {
                    let mut p = "";

                    match i {
                        0 => p = "0", // left node
                        1 => p = "1", // right node
                        _ => (),
                    }

                    child.encode(Some(&format!("{}{}", prefix, p)));
                }
            },
        }
    }
}

fn internal_node(priority_queue: &mut BinaryHeap<Node>, arity: usize) -> Node {
    let children = priority_queue.pop_by(arity);
    let priority = children.iter().fold(0, |acc, child| acc + child.priority());

    Node::Internal { children: children, priority: priority }
}

fn create_tree(elements: &[Node]) -> Option<Node> {
    let elements = Vec::from(elements);
    let arity = 2;
    let mut priority_queue = BinaryHeap::new();

    priority_queue.extend(elements.into_iter());

    while priority_queue.len() > 1 {
        let internal_node = internal_node(&mut priority_queue, arity);

        priority_queue.push(internal_node);
    }

    priority_queue.pop()
}

fn frequencies(string: &str) -> HashMap<char, usize> {
    let mut letters = HashMap::new();

    for ch in string.chars() {
        let counter = letters.entry(ch).or_insert(0);
        *counter += 1;
    }

    letters
}

fn map_nodes(chars: HashMap<char, usize>) -> Vec<Node> {
    chars.into_iter().map(|(ch, pr)| Node::Leaf { value: ch, priority: pr }).collect()
}

#[test]
fn test_freq() {
    assert_eq!(frequencies("this is an example for huffman encoding").len(), 19);
}

#[test]
fn trying_out_pq() {
    let letters = frequencies("this is an example for huffman encoding");
    let nodes = map_nodes(letters);
    let root = create_tree(&nodes).unwrap();

    assert_eq!(root.priority(), 39);
    root.encode(None);
}

#[test]
fn when_0_elements_returns_none() {
    let v: Vec<Node> = Vec::new();
    let root = create_tree(&v);

    assert_eq!(root, None);
}

#[test]
fn when_1_element_returns_1_leaf_node() {
    let value = 'a';
    let priority = 15;
    let v = vec![Node::Leaf { value: value, priority: priority }];
    let root = create_tree(&v).unwrap();

    assert_eq!(root.value(), value);
    assert_eq!(root.priority(), priority);
}
